template PythagorasVerifier() {
    signal private input a;
    signal private input b;
    signal private input c;
    signal output out;

    signal a_squared;
    signal b_squared;
    signal c_squared;

    a_squared <== a * a;
    b_squared <== b * b;
    c_squared <== c * c;

    c_squared <== a_squared + b_squared;
    var res = 0;
    if (a * a + b * b == c * c) {
        res = 1;
    }
    out <== res;
}
component main = PythagorasVerifier();
