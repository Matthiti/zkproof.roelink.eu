module.exports = {
  css: {
    extract: true,
    loaderOptions: {
      sass: {
        prependData: '@import "@/scss/_variables.scss";'
      }
    }
  }
}
